import { Component } from '@angular/core';
import {Location} from '@angular/common';
import { faGithub } from '@fortawesome/free-brands-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  faGithub = faGithub;
  faHome = faHome;

  constructor(private location: Location)
  {}

  gitClicked(event: Event) {
    window.open('https://github.com/Wauchy/popcorn', '_blank');
  }
}
