import { TestBed, async } from '@angular/core/testing';
import { NavbarComponent } from './navbar.component';

describe('Navbar', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavbarComponent
      ],
    }).compileComponents();
  }));

  it('should create the navbar', () => {
    const fixture = TestBed.createComponent(NavbarComponent);
    const navbar = fixture.debugElement.componentInstance;
    expect(navbar).toBeTruthy();
  });

});
