import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from "@angular/common/http";
import { OMDBApiService } from "../services/omdb-api.service";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [HttpClient, OMDBApiService],
})
export class MovieDetailModule { }
