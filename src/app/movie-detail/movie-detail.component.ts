import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { OMDBApiService } from '../services/omdb-api.service';
import { trigger, style, animate, transition } from '@angular/animations';
import { faImdb } from '@fortawesome/free-brands-svg-icons';
import { faClock } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-movie',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(2000, style({opacity: 1}))
      ])
    ])
  ]
})
export class MovieDetailComponent implements OnInit, OnDestroy {
  movie;
  movieId = '';
  fullDialogImagePoster = false;
  faImdb = faImdb;
  faClock = faClock;

  constructor(
    private activatedRoute: ActivatedRoute,
    private appTitle: Title,
    private apiService: OMDBApiService
  ) { }

  ngOnInit() {
    this.movieId = this.activatedRoute.snapshot.params.id;

    this.movie = this.apiService.getMovieById(this.movieId).subscribe((data) => {

      this.movie = data;

      this.appTitle.setTitle(this.movie['Title']);
    });
  }

  ngOnDestroy() {
  }

  fullImagePoster() {
    this.fullDialogImagePoster = !this.fullDialogImagePoster;
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === 27){
      this.fullImagePoster();
    }
  }
}
