import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export const OMDBApiSettings = {
  production: false,
  apiKey: '89d31ccc',
  apiUrl: 'https://www.omdbapi.com'
};

@Injectable({
  providedIn: 'root'
})
export class OMDBApiService {

  constructor(
    private http: HttpClient
  ) { }

  public searchMovies(url) {
    return this.http.get(`${OMDBApiSettings.apiUrl}/?apikey=${OMDBApiSettings.apiKey}` + url + '&type=movie');
  }

  public startMovieList(startQuery) {
    return this.http.get(`${OMDBApiSettings.apiUrl}/?apikey=${OMDBApiSettings.apiKey}` + startQuery + 'type=movie');
  }

  public getMovieById(movieId) {
    return this.http.get(`${OMDBApiSettings.apiUrl}/?apikey=${OMDBApiSettings.apiKey}&i=${movieId}&type=movie&plot=full`);
  }
}
