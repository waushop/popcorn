import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import { MoviesComponent } from "./movies.component";
import { MovieDetailComponent } from "../movie-detail/movie-detail.component";
import { MaterialModule } from "../material.module"

@NgModule({
  declarations: [
    MoviesComponent,
    MovieDetailComponent,
  ],
  imports: [
    CommonModule, 
    FormsModule, 
    HttpClientModule, 
    MaterialModule,
  ],
  providers: [HttpClientModule, HttpClient],
})
export class MoviesModule {}
