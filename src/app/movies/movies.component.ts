import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OMDBApiService } from '../services/omdb-api.service';
import { Title } from '@angular/platform-browser';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-search',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(2000, style({opacity: 1}))
      ])
    ])
  ]
})
export class MoviesComponent implements OnInit {
  form: FormGroup;
  movieList;
  visibleMovieList;
  searchResults;
  totalResults;
  paginationCounter = 1;
  startQueryParameters = '&s=batman&plot=full';
  searchParameters = '';
  sortBy: any = 'Sort by';
  noScroll = true;
  showTotalResultsSection = true;

  constructor(
    public fb: FormBuilder,
    private moviesService: OMDBApiService,
    private title: Title
  ) {
    this.form = this.fb.group({
      title: [''],
      year: [''],
    });
  }

  ngOnInit() {
    this.title.setTitle('Popcorn');
    this.searchParameters = this.startQueryParameters;
    if (sessionStorage.getItem('searchResults') === null) {
      this.moviesService.startMovieList(this.startQueryParameters).subscribe((data) => {
        this.totalResults = data['totalResults'];
        this.movieList = data['Search'];
      });
    }
    this.movieList = JSON.parse(sessionStorage.getItem('searchResults'));
    this.totalResults = JSON.parse(sessionStorage.getItem('totalResults'));
  }

  submitForm() {
    this.paginationCounter = 1;
    this.searchParameters = '';

    let url = '';

    if (this.form.value.title) {
      url += '&s=' + this.form.value.title;
    }

    if (this.form.value.year) {
      url += '&y=' + this.form.value.year;
    }

    this.searchParameters = url;

    this.moviesService.searchMovies(url).subscribe((data) => {
      if (data['Response'] === 'False') {
        this.showTotalResultsSection = false;
        this.movieList = [];
      }

      if (data['Response'] === 'True') {
        this.movieList = data['Search'];
        this.totalResults = data['totalResults'];
      }
      sessionStorage.removeItem('searchResults');
      sessionStorage.removeItem('totalResults');
      sessionStorage.setItem('searchResults', JSON.stringify(this.movieList));
      sessionStorage.setItem('totalResults', JSON.stringify(this.totalResults));

    });
    window.scroll(0, 0);
  }

  onScroll() {
    if (this.noScroll) {
      this.noScroll = false;
      this.loadNextData();
    }
  }

  loadNextData() {
    const url = this.searchParameters + `&page=${++this.paginationCounter}`;

    if (this.movieList.length < this.totalResults) {
      this.moviesService.searchMovies(url).subscribe((data) => {
        this.movieList = [...this.movieList, ...data['Search']];
        sessionStorage.setItem('searchResults', JSON.stringify(this.movieList));
        sessionStorage.setItem('totalResults', JSON.stringify(this.totalResults));
      });
    }
    this.noScroll = true;
  }

  sortMovies() {
    const sortValue = this.sortBy;
    this.movieList.sort((a: any, b: any) => {
      if (a[sortValue] > b[sortValue]) {
        return -1;
      } else if (a[sortValue] < b[sortValue]) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
