# Popcorn - Movie Search :movie_camera:

Popcorn is a movie search application built with @Angular and it uses OMDb API to get detailed information and poster images.

- This application displays a list of movies. 
- Movies can be searched by their titles by using the search bar, results can be filtered by year. 
- Searching movies only by year without entering a title is not possible. 
- Search results can be sorted by title alphabetically and release year.
- Total count of search results is displayd on search button.

Every movie has it's own detailed information page where you can get by clicking on it's poster in search results.

### [Demo](https://popcorn.vaus.ee/)

# Technologies used

- [Angular](https://angular.io/)
- [Angular Material](https://material.angullar.io/)
- [TypeScript](https://www.typescriptlang.org/)
- [Rxjs](https://github.com/ReactiveX/rxjs)
- [Sass](http://sass-lang.com/)
- [Bootstrap](http://getbootstrap.com/)
- [FortAwesome](https://fortawesome.com/)

# Quick start

### Clone this repository

```bash
git clone https://github.com/Wauchy/popcorn.git
```
### Navigate to project folder


```bash
cd popcorn
```

### Install dependencies

```bash
npm install
```

### Start development server

```bash
ng serve
```

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)


